from typing import Optional
import logging

from fastapi import FastAPI
import sentry_sdk
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware
from sentry_sdk.integrations.logging import LoggingIntegration

# All of this is already happening by default!
sentry_logging = LoggingIntegration(
    level=logging.INFO,  # Capture info and above as breadcrumbs
    event_level=logging.ERROR,  # Send errors as events
)

sentry_sdk.init(
    dsn="http://64428f409edd409c847fdd21a9ce113c@54.38.65.44/3",
    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=1.0,
    integrations=[sentry_logging],
)
sentry_sdk.set_context(
    "Super context",
    {"name": "My ultra good application", "version": "1.1.0", "Misc": "Yo my name is Go"},
)

app = FastAPI()

asgi_app = SentryAsgiMiddleware(app)


@app.get("/")
async def read_root():
    print("hahaha")
    logging.error("error")
    logging.info("info")
    logging.warning("warning")
    return {"Hello": "World"}


@app.get("/items/{item_id}")
async def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}
